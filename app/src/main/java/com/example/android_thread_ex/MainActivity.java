package com.example.android_thread_ex;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private Button btn;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageDownloader imageDownloader = new ImageDownloader();
                imageDownloader.start();
            }
        });

    }

    class ImageDownloader extends Thread{
        String imageURLString = "https://klike.net/uploads/posts/2019-05/1556708032_1.jpg";
        Bitmap imageBitmap;
        @Override
        public void run() {
            try {
                URL imageURL = new URL(imageURLString);
                imageBitmap =  (Bitmap) imageURL.getContent();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        img.setImageBitmap(imageBitmap);
                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}